
const { Client } = require("pg");
let QueriesCitations = require("./queries/queries_citations");
let QueriesUsers = require("./queries/queries_users");
let QueriesParams = require("./queries/queries_params");

let testConnection = "postgres://nhnysdjvupizlr:a089fe6fd11d710bfdd51a335dedf5ed926b5d4625f3593c57d2649e5befd6aa@ec2-54-175-117-212.compute-1.amazonaws.com:5432/d2or5moq8ochra"

function DB () {
    
    /* Fonction de connection a la base de donnees */
    this.connect = function () {
        this.client = new Client({
            connectionString: testConnection,
            ssl: {
                rejectUnauthorized: false
            }
        });
        this.client.connect();
    }

    this.disconnect = function () {
        this.client.end();
    }

    /*Retrouve toutes les citations.
    Appel onSuccess avec un tableau contenant ces citations en cas de succes.
    Appel onFail avec une erreur en parametre en cas d'erreur.*/
    this.getAllCitations = function (onSuccess, onFail) {
            this.connect();
            this.client.query(
                QueriesCitations.getGetAllCitationsQuery(),
                (err, res) => {
                    if(err) {
                        throw err; // Test
                        onFail(err);
                    }
                    else {
                        onSuccess(res.rows);
                    }
                }
            );
        }
    /*Retrouve toutes les citations crees apres la date "date".
    Appel onSuccess avec un tableau contenant ces citations en cas de succes.
    Appel onFail avec une erreur en parametre en cas d'erreur.*/
    this.getCitationsAfter = function (date, onSuccess, onFail) {
            this.connect();
            this.client.query(
                QueriesCitations.getGetCitationsAfterQuery(date),
                (err, res) => {
                    if(err) {
                        throw err; // Test
                        onFail(err);
                    }
                    else {
                        onSuccess(res.rows);
                    }
                }
            );
        }
    /*Ajoute une citation de contenu "content", appartenant a l'user de token "userToken".
    Appel onSuccess  en cas de succes.
    Appel onFail avec une erreur en parametre en cas d'erreur.*/
    this.addCitation = function (content, userToken, onSuccess, onFail) {
            this.connect();
            this.client.query(
                QueriesCitations.getAddCitationQuery(content, userToken),
                (err, res) => {
                    if(err) {
                        throw err; // Test
                        onFail(err);
                    }
                    else {
                        onSuccess();
                    }
                }
            );
        }
    /*Retrouve une citation aleatoire de la base de donnees
    qui n'appartient pas a l'utilisateur d'id "userid".
    Appel onSuccess avec cette citation en cas de succes.
    Appel onFail avec une erreur en parametre en cas d'erreur.
    Si aucune citations n'a pu etre trouvee, l'erreur contient un champ "nothing" de valeur true.*/
    this.getRandomCitationForUser = function (userid, onSuccess, onFail) {
            this.connect();
            this.client.query(
                QueriesCitations.getGetRandomCitationRequestForUserQuery(userid),
                (err, res) => {
                    if(err) {
                        throw err; // Test
                        onFail(err);
                    }
                    else {
                        if(res.rows.length <= 0) {
                            let err = {};err.nothing = true;
                            onFail(err);
                        }
                        else {
                            onSuccess(res.rows[0]);
                        }
                    }
                }
            );
        }

    /*Retrouve l'utilisateur de login et password passes en parametre.
    Appel onSuccess avec l'utilisateur en paramtre en cas de succes.
    Appel onFail avec une erreur en parametre en cas d'erreur.
    Si aucun utilisateur n'a pu etre trouvee, l'erreur contient un champ "nothing" de valeur true.*/
    this.getUser = function (login, password, onSuccess, onFail) {
        this.connect();
        this.client.query(
            QueriesUsers.getUserByLoginPasswordQuery(login, password),
            (err, res) => {
                if(err) {
                    throw err; // Test
                    onFail(err);
                }
                else {
                    if( res.rows.length === 0 ) {
                        let err = {};err.nothing = true;
                        onFail(err);
                    }
                    else {
                        onSuccess(res.rows[0]);
                    }
                }
            }
        );
    }

    /*Retrouve l'utilisateur de token "token".
    Appel onSuccess avec l'utilisateur en paramtre en cas de succes.
    Appel onFail avec une erreur en parametre en cas d'erreur.
    Si aucun utilisateur n'a pu etre trouvee, l'erreur contient un champ "nothing" de valeur true.*/
    this.getUserByToken = function (token, onSuccess, onFail) {
            this.connect();
            console.log("Requete : ")
            console.log(QueriesUsers.getUserByTokenQuery(token))
            this.client.query(
                QueriesUsers.getUserByTokenQuery(token),
                (err, res) => {
                    if(err) {
                        throw err; // Test
                        onFail(err);
                    }
                    else {
                        if(res.rows.length <= 0) {
                            let err = {};err.nothing = true;
                            onFail(err);
                        }
                        else {
                            onSuccess(res.rows[0]);
                        }
                    }
                }
            );
        }
    /*Ajoute a la base de donnees un utilisateur de login et password passes en parametre.
    Appel onSuccess en cas de succes.
    Appel onFail avec une erreur en parametre en cas d'erreur.*/
    this.addUser = function (login, password, onSuccess, onFail) {
            this.connect();
            this.client.query(
                QueriesUsers.getAddUserQuery(login, password),
                (err, res) => {
                    if(err) {
                        throw err; // Test
                        onFail(err);
                    }
                    else {
                        this.getUser(
                            login, password,
                            (user) => {
                                onSuccess(user);
                            },
                            (err) => {
                                onFail(err)
                            })
                    }
                }
            );
        }
    /*Change le token de l'utilisateur d'id "userid" en "token".
    Appel onSuccess en cas de succes.
    Appel onFail avec une erreur en parametre en cas d'erreur.*/
    this.setToken = function (token, userid, onSuccess, onFail) {
            this.connect();
            this.client.query(
                QueriesUsers.getSetUserTokenQuery(token, userid),
                (err, res) => {
                    if(err) {
                        throw err; // Test
                        onFail(err);
                    }
                    else {
                        onSuccess();
                    }
                }
            )
        }

    /*Change la valeur du token maximal en "token".
    Appel onSuccess en cas de succes.
    Appel onFail avec une erreur en parametre en cas d'erreur.*/
    this.setMaxToken = function (token, onSuccess, onFail) {
            this.connect();
            this.client.query(
                QueriesParams.getSetMaxTokenQuery(token),
                (err, res) => {
                    if(err) {
                        throw err; // Test
                        onFail(err);
                    }
                    else {
                        onSuccess();
                    }
                }
            );
        }
    /*Retrouve le token maximal.
    Appel onSuccess avec ce token en paramtre en cas de succes.
    Appel onFail avec une erreur en parametre en cas d'erreur.
    Si aucun Params (qui contien le champ maxToken) n'a pu etre trouvee, l'erreur contient un champ "nothing" de valeur true.*/
    this.getMaxToken = function (onSuccess, onFail) {
            this.connect();
            this.client.query(
                QueriesParams.getGetMaxTokenQuery(),
                (err, res) => {
                    if(err) {
                        throw err; // Test
                        onFail(err);
                    }
                    else {
                        if(res.rows.length >= 0) {
                            onSuccess(res.rows[0].maxtoken);
                        }
                        else {
                            let err = {};err.nothing = true;
                            onFail(err);
                        }
                    }
                }
            );
        }
}

let db = new DB()
db.connect();
module.exports = db;

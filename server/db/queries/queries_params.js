
/* Pour creer la table de parametres */
let CREATE_PARAMS = 
    "CREATE TABLE Params ( id INT PRIMARY KEY, maxtoken INT DEFAULT 0);";
let getCreateParamsQuery = function () {
    return {
        text : CREATE_PARAMS
    };
}

/* Pour initialiser les parametres */
let INIT_MAX_TOKEN =
    "INSERT INTO Params (id, maxtoken) VALUES (1, 0);"
let getInitParamsQuery = function () {
    return {
        text : INIT_MAX_TOKEN
    }
}

/* Pour changer de maxtoken */
let SET_MAX_TOKEN = 
    "UPDATE Params SET maxtoken = $1 WHERE id = 1;";
let getSetMaxTokenQuery = function (token) {
    return {
        text : SET_MAX_TOKEN,
        values : [token]
    }
}

/* Pour obtenir maxtoken */
let GET_MAX_TOKEN =
    "SELECT maxtoken FROM Params WHERE id=1;";
let getGetMaxTokenQuery = function () {
    return {
        text : GET_MAX_TOKEN
    }
}

module.exports = {
    getCreateParamsQuery,
    getInitParamsQuery,
    getSetMaxTokenQuery,
    getGetMaxTokenQuery
}
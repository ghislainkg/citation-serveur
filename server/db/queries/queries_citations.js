
/* Pour creer la table Citations */
let CREATE_CITATION =
    "CREATE TABLE Citations (" +
        "id SERIAL PRIMARY KEY NOT NULL," +
        "content TEXT," +
        "creationdata DATE," +
        "owner INT," +

        "FOREIGN KEY (owner) " +
            "REFERENCES Users(id) " +
            "ON DELETE CASCADE);";
let getCreateCitationsQuery = function ()  {
    return {
        text : CREATE_CITATION
    };
}

/* Pour obtenir toutes les citations */
let GET_CITATIONS = "SELECT * FROM Citations;";
let getGetAllCitationsQuery = function () {
    return {
        text : GET_CITATIONS
    };
}

/* Pour obtenir toutes les citations crees apres une date */
let GET_CITATION_AFTER =
    "SELECT * FROM Citations " +
    "WHERE " +
            "Citations.creationdata > $1; ";
let getGetCitationsAfterQuery = function (date) {
    return {
        text : GET_CITATION_AFTER,
        values : [data]
    }
}

/* Pour ajouter une citation */
let ADD_CITATION = 
    "INSERT INTO Citations (content, creationdata, owner) " +
    "SELECT $1, NOW(), Users.id FROM Users WHERE Users.token = $2;";
let getAddCitationQuery = function (content, userToken) {
    return {
        text : ADD_CITATION,
        values : [content, userToken]
    }
}

/* Pour obtenir une citation  aleatoire */
let RANDOM_CITATION = 
"SELECT * FROM Citations " +
"WHERE owner != $1 "+
"LIMIT 1 OFFSET (SELECT (CAST(RANDOM()*100000 AS INT) % (1+(SELECT COUNT(*) FROM Citations)) ));"
let getGetRandomCitationRequestForUserQuery = function (userid) {
    return {
        text : RANDOM_CITATION,
        values : [userid]
    }
}

module.exports = {
    getCreateCitationsQuery,
    getGetAllCitationsQuery,
    getGetCitationsAfterQuery,
    getAddCitationQuery,
    getGetRandomCitationRequestForUserQuery
}
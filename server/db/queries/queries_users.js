
/* Pour creer la table Users */
let CREATE_USERS = 
    "CREATE TABLE Users (" +
        "id SERIAL PRIMARY KEY NOT NULL," +
        "login VARCHAR UNIQUE NOT NULL," +
        "password VARCHAR NOT NULL," +
        "publickey VARCHAR," +
        "token INT UNIQUE);";
let getCreateUsersQuery = function () {
    return {
        text : CREATE_USERS
    };
}

/* Pour obtenir un utiliateur avec sont login et password */
let GET_USER =
    "SELECT * FROM Users " + 
    "WHERE " + 
        "Users.login = $1 AND " +
        "Users.password = $2;";
let getUserByLoginPasswordQuery = function (login, password) {
    return {
        text : GET_USER,
        values : [login, password]
    }
}

/* Pour obtenir un utilisateur avec son token */
let GET_USER_BY_TOKEN = 
    "SELECT * FROM Users " +
    "WHERE " +
        "Users.token = $1;";
let getUserByTokenQuery = function (token) {
    return {
        text : GET_USER_BY_TOKEN,
        values : [token]
    }
}

/* Pour ajouter un utilisateur */
let ADD_USER = 
    "INSERT INTO Users (login, password) VALUES ($1, $2);"
let getAddUserQuery = function (login, password) {
    return {
        text : ADD_USER,
        values : [login, password]
    }
}

/* Pour mettre a jour un utilisateur */
let SET_USER_TOKEN =
    "UPDATE Users SET token = $1 WHERE id = $2 ;"
let getSetUserTokenQuery = function (token, userid) {
    return {
        text : SET_USER_TOKEN,
        values : [token, userid]
    }
}
module.exports = {
    getCreateUsersQuery,
    getUserByLoginPasswordQuery,
    getUserByTokenQuery,
    getAddUserQuery,
    getSetUserTokenQuery
}

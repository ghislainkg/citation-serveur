const { Client } = require("pg");
let QueriesCitations = require("./queries/queries_citations");
let QueriesUsers = require("./queries/queries_users");
let QueriesParams = require("./queries/queries_params");

/*Ce fichier permet juste de modifier la structure de la base de donnees remote*/
/* Les structures definies dans "queries/" sont en voyees en la base de donnees est videe*/

let testConnection = "postgres://nhnysdjvupizlr:a089fe6fd11d710bfdd51a335dedf5ed926b5d4625f3593c57d2649e5befd6aa@ec2-54-175-117-212.compute-1.amazonaws.com:5432/d2or5moq8ochra"

/* On se connecte a la base de donnees.*/
let client = new Client({
    connectionString: testConnection,
    ssl: {
        rejectUnauthorized: false
    }
});
client.connect();

/* Creation des nouvelles tables */
function create () {
    console.log("Start create Users");
    client.query(
        QueriesUsers.getCreateUsersQuery(),
        (err, res) => {
            if(err) throw err;

            console.log("Start create Citations");
            client.query(
                QueriesCitations.getCreateCitationsQuery(),
                (err, res) => {
                    if(err) throw err;

                    console.log("Start create Params");
                    client.query(
                        QueriesParams.getCreateParamsQuery(),
                        (err, res) => {
                            if(err) throw err;

                            console.log("Start adding max token");
                            client.query(
                                QueriesParams.getInitParamsQuery(),
                                (err, res) => {
                                    if(err) throw err;

                                    client.end();
                                }
                            )
                        }
                    )
                }
            )
        }
    );
}

/* Destruction des tables */
client.query(
    {
        text : "DROP TABLE Citations;"
    },
    (err, res) => {

        client.query(
            {
                text : "DROP TABLE Users;"
            },
            (err, res) => {

                client.query(
                    {
                        text : "DROP TABLE Params;"
                    },
                    (err, res) => {

                        console.log("End dropping");

                        create();
                    }
                );
            }
        );
    }
);


/* Ce fichier defini la premiere etape du traitement d'un message entrant :
- dechiffrage
- identification du type
- extraction du contenu
- verification du token*/

var Cipher = require("./aes/cipher");
var cipher = new Cipher();

var db = require("./db/pghandler");

/*
    msg format :
        type|token|content
*/

/* Extrait le type, token et contenu du message msg bien formate.
Retourne 0 si le msg n'est pas bien formate.*/
function extractRequest(msg) {
    console.log("Message : "+msg)
    let format = msg.split("|");
    console.log(format)
    let type = format[0];
    let token = format[1];
    let content = format[2];

    console.log("Type = "+type);
    console.log("Token = "+token);
    console.log("Content = "+content);
    
    if(typeof(type) == "string" && typeof(token) == "string" && typeof(content) == "string") {
        if(type.length == 1) {
            let tokenInt = parseInt(token);
            console.log("Token :")
            console.log(tokenInt)
            console.log(typeof(tokenInt))
            if(typeof(tokenInt) != "number" || isNaN(tokenInt)) {
                return 0;
            }
            return {
                type,
                token : tokenInt,
                content
            }
        }
        else {
            return 0;
        }
    }
    else {
        return 0;
    }
}

/* Verifi qu'il existe un utilisateur possedant le token "token".
Se cette utilisateur existe, callback est appeller avec lui comme parametre,
Si non, le parametre de callback est false.
En cas d'erreur, res vas permetre d'envoyer un status d'erreur 500.*/
function checkToken(token, res, callback) {
    db.getUserByToken(
        token,
        function (user) {
            callback(user);
        },
        function (err) {
            console.log(err)
            console.log("500")
            res.sendStatus(500); // Internal error
        }
    );
}

/* Ce middleware valide le message contenu dans req.body :*/
let validateMessage = function (req, res, next) {
    // Pour valider une message, ...
    console.log("Message to validate");
    var body = req.body;
    console.log(body);
    // On commence par le dechiffrer et pour cas, on initialise le cipher
    cipher.init(
        () => {
            // Si l'initialisation s'est bien passee, 
            console.log("Cipher result");
            // On dechiffre le message
            let msg = cipher.decrypt(body);
            if(msg == 0) {
                // Si le dechiffrement ne s'est pas bien passe,
                // on envoi un message d'erreur au client.
                console.log("Couldn't decipher message");
                res.send(400); // Bad request
                return;
            }
            console.log("Deciphered message : " + msg);
            // On extrait les information du msg dechiffre
            request = extractRequest(msg);
            if(request == 0) {
                console.log("Request not formated")
                // Si le msg n'est pas bien formate,
                // On envoie une erreur
                res.send(400); // Bad request
                return;
            }
            // On stocke la requete contenu dans le msg dans req.
            req.citation = {
                request
            }

            console.log("Type = "+request.type);
            console.log("Token = "+request.token);
            console.log("Content = "+request.content);

            // En fonction du type de la requete,
            // on effectu l'action appropriee
            if(request.type == "c") {
                // Si c'est une requete de connection,
                // On suppose que la route demandee par l'utilisateur est celle de connexion,
                // Donc on passe a la suite qui est la connexion
                console.log("Connect request");
                next(); // Connection request
            }

            else if(request.type == "n") {
                // Si c'est une requete de creation de nouvel utilisateur,
                // On suppose que la route demandee par l'utilisateur est celle de creation d'utilisateur,
                // Donc on passe a la suite qui est la creation de nouvel utilisateur
                console.log("New user request");
                next();
            }

            else if(request.type == "d") {
                // Si c'est une requete de deconnection,
                // on verifi que celui qui fait cette requete existe bien.
                console.log("Disconnect request");
                checkToken(req.citation.request.token, res, (user) => {
                    if(user === false) {
                        // S'il nexiste pas, on envoie une erreur
                        console.log("Bad token");
                        console.log("401")
                        res.send(401); // Not authorized
                    }
                    else {
                        // S'il existe, on l'ajoute a req,
                        // Et on passe a la suite qui  devrait etre sur la route de deconnexion
                        console.log("right token");
                        req.citation.user = user;
                        next() // Disconnection request
                    }
                });
            }

            else if(request.type == "a") {
                // Si c'est une requete d'ajout de citation, 
                // On verifi que celui qui fait cette requete est bien en cours de communication.
                console.log("Add citation request");
                checkToken(req.citation.request.token, res, (user) => {
                    if(user === false) {
                        // S'il ne l'est pas, on envoie une erreur
                        console.log("401")
                        res.send(401); // Not authorized
                    }
                    else {
                        // S'il l'est, on l'ajoute a req
                        console.log("Next")
                        req.citation.user = user;
                        next()
                    }
                });
            }

            else if(request.type == "r") {
                // Si c'est une requete de citation aleatoire,
                // On verifie que le client est bien en cours de communication
                console.log("Get citation request");
                checkToken(req.citation.request.token, res, (user) => {
                    if(user === false) {
                        // S'il ne l'est pas, on envoie une erreur
                        console.log("401")
                        res.send(401); // Not authorized
                    }
                    else {
                        // S'il est, on l'ajoute a req
                        req.citation.user = user;
                        next()
                    }
                });
            }

            else {
                // Si la requete est inconnue, on envoie une erreur
                console.log("Unknown request");
                res.send(400); // Bad request
            }
        },
        (err) => {
            // S'il y a eu une erreur lors du dechiffrage
            console.log("Cipher error");
            res.send(500); // Internal error
            throw err;
        }
    );
}

module.exports = validateMessage;
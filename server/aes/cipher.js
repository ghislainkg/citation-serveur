let crypto = require("crypto");
let fs = require("fs")

/* Cette fonction lit les fichiers de nom keyfile, ivfile 
et les donnes en argument de onResult.*/
function readKeys (keyfile, ivfile, onResult, onFail) {
    // On lit le ficheir de la cle
    fs.readFile(keyfile, (err, key) => {
        if(err) {
            throw err; // Test
            onFail(err);
        }
        else {
            // On lit le fichier de la matrice
            fs.readFile(ivfile, (err, iv) => {
                if(err) {
                    throw err; // Test
                    onFail(err);
                }
                else {
                    onResult(key, iv);
                }
            })
        }
    });
}

/* Classe pour cripter des messages*/
function Cipher () {
    // Les buffer contenant la cle et la matrice d'initialisation
    this.keyBuf = Buffer.allocUnsafe(32);
    this.ivBuf = Buffer.allocUnsafe(16);
    
    /* Retourne le message msg cripte. Retourne 0 en cas d'erreur*/
    this.encrypt = function (msg) {
        let res = 0;
        try {
            let cipher = crypto.createCipheriv("aes-256-cbc", this.keyBuf, this.ivBuf);
            cipher.setAutoPadding(true);
            var c = cipher.update(msg, "utf8", "base64");
            res = c+cipher.final("base64");
        }
        catch(error) {
            console.log(error);
            return 0;
        }
        return res;
    }

    /* Retourne le message msg decripter. Retourne 0 en cas d'erreur*/
    this.decrypt = function (msg) {
        let res = 0;
        try {
            let decipher = crypto.createDecipheriv("aes-256-cbc", this.keyBuf, this.ivBuf);
            decipher.setAutoPadding(false);
            res = decipher.update(msg, "base64", "utf8");
        }
        catch(error) {
            throw error; // Test
            console.log(error);
            return 0;
        }
        return res;
    }

    /* Initialise les buffers de la cle et de la matrice d'initialisation */
    this.init = function (onReady, onFail) {
        readKeys(
            // Les fichiers contenant la cle et la matrice d'initialisation
            "./server/aes/keys/key",
            "./server/aes/keys/iv",
            //"./keys/key",
            //"./keys/iv",
            (privateKey, iv) => {

                // On hashe la matrice d'initialisation
                let ivBufAux = crypto.createHash("sha256")
                    .update(iv)
                    .digest();
                ivBufAux.copy(this.ivBuf);

                // On hashe la cle
                let keyBufAux = crypto.createHash("sha256")
                    .update(privateKey)
                    .digest();
                keyBufAux.copy(this.keyBuf);

                let ivStr = this.ivBuf.toString();
                console.log("iv = "+ivStr);

                let keyStr = this.keyBuf.toString();
                console.log("key = "+keyStr);

                // On est pret
                onReady();
            },
            (err) => {
                throw err; // Test
                console.log(err)
                onFail(err);
            }
        );
    }
}

module.exports = Cipher;
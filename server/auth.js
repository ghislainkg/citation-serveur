let db = require("./db/pghandler")

/*
    Authentification request content:
        |login@password|
*/

/* Cette fonction genere une nouveau token a partir du token max actuel.
callback est appele avec comme parametre le nouveau token.
en cas d'erreur, le parametre de callback est false.*/
function getNewToken(callback) {
    // On commence par aller chercher le token max
    db.getMaxToken(
        (maxToken) => {
            // On l'a, on l'incremente
            console.log("Max token :"+maxToken)
            newToken = ++maxToken;
            console.log("\n\n\n\n\n\nSetting max token "+newToken);
            // Et on met a jour le nouveau token maximal
            db.setMaxToken(
                newToken,
                () => {
                    // La mise a jouur s'est bien passee
                    callback(newToken);
                },
                (err) => {
                    // Une erreur est survenue
                    throw err; // Dev
                    callback(false);
                }
            );
        },
        (err) => {
            // On n'a pas le token max, on
            throw err; // Dev
            callback(false);
        }
    );
}

/* Cette function estrait du contenu d'un message
Le login et le mot de passe d'un utilisateur
qu'il soit deja existant on pas.
Si le contenu n'est pas bien formate, retourne 0.*/
function extractUser(content) {
    let format = request.content.split("@");
    let login = format[0];
    let password = format[1];

    if(typeof(login) == "string" && typeof(password) == "string") {
        return {
            login,
            password
        }
    }
    else {
        return 0;
    }
}

/* Ce middleware connecte une utilisateur.
Il ajout cet utilisateur avec son nouveau token a req.
Il doit etre appele apres la validation de message.*/
let connect = function (req, res, next) {
    // La requete obtenu apres validation du message
    let request = req.citation.request;

    console.log(request);

    // On verifi le type de la requete
    if(request.type != "c") {
        next();
        return;
    }

    // On recupere les identifiants 
    let credential = extractUser(request.content);
    if(credential == 0) {
        // Si le contenu n'est pas un login et un password
        res.sendStatus(400); // Bad message
    }

    console.log("Login = "+credential.login);
    console.log("Password = "+credential.password);

    // On recupere l'utilisateur
    db.getUser(
        credential.login,
        credential.password,
        function (user) {
            // Une fois l'utilisateur retourne, on genere un nouveau token pour lui
            getNewToken((newToken) => {
                if(newToken === false) {
                    res.sendStatus(500); // internal error
                }
                else {
                    // Une fois qu'on a ce nouveau token,
                    // on change le token de l'utilisateur dans la db
                    db.setToken(newToken, user.id);
                    // On ajoute l'utilisateur mis a jour a req,
                    user.token = newToken;
                    req.citation.user = user;
                    // On passe a la suite
                    next();
                }
            });
        },
        function (err) {
            // Si one n'a pas pu avoir l'utilisateur
            throw err;
            if(err.nothing != undefined) {
                // parce que l'utilisateur n'existe pas
                res.sendStatus(401); // Not authorized
            }
            else {
                // parce qu'une erreur s'est produite
                res.sendStatus(500); // internal error
            }
        }
    )
}

/* Ce middleware cree un nouvel utilisateur.
Il ajoute cet utilisateur avec une nouveau token a req.
Il doit etre appele apres la validation de message.*/
let newuser = function (req, res, next) {
    let request = req.citation.request;

    console.log(request);

    if(request.type != "n") {
        next();
        return;
    }

    // On recupere les identifiants 
    let credential = extractUser(request.content);
    if(credential == 0) {
        // Si le contenu n'est pas un login et un password
        res.sendStatus(400); // Bad message
    }

    console.log("Login = "+credential.login);
    console.log("Password = "+credential.password);

    // On ajoute le nouveau utilisateur
    db.addUser(
        credential.login,
        credential.password,
        (user) => {
            // Si l'utilisateur a bien ete ajoute, on lui genere un nouveau token
            getNewToken((newToken) => {
                if(newToken === false) {
                    // Si le token n'a pas pu etre genere
                    console.log("No new token");
                    res.sendStatus(500); // internal error
                }
                else {
                    // Si le token a pu etre genere
                    console.log("New token = "+newToken);
                    // On met a jour le token de l'utilisateur dans la db
                    db.setToken(
                        newToken, user.id,
                        () => {
                            // Et on l'ajoute avec son nouveau token a req.
                            user.token = newToken;
                            req.citation.user = user;
                            next();
                        },
                        () => {
                            // On n'a pas pu changer le token de l'utilisateur
                            res.sendStatus(500); // internal error
                        });
                }
            });
        },
        (err) => {
            // Si on n'a pas pu ajouter le nouvel utilisateur.
            console.log(err);
            throw err;
            res.sendStatus(500); // Internale error
        }
    );
}

module.exports = {
    connect,
    newuser
};
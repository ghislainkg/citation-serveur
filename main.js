let express = require("express");
let bodyParser = require("body-parser");

let validate = require("./server/validate");
let Cipher = require("./server/aes/cipher");
let auth = require("./server/auth");
let db = require("./server/db/pghandler");

let cipher = new Cipher();

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text({ type : "text/plain" }));

app.use(validate);

/* Nous recevons une requete de connection ou de creation de nouvel utilisateur.*/
app.use("/connect", auth.connect);
app.use("/connect", auth.newuser);
app.all("/connect", (req, res) => {
	// L'utilisateur est dans la db
	console.log("Nouvelle connections");
	let user = req.citation.user;
	// On lui envoie son nouveau token en le chiffrant
	// Pour ca, on initialise un cipher
	cipher.init(
		() => {
			// Le cipher est initialise, on encrypte le token.
			let toSend = cipher.encrypt(user.token+"");
			console.log("Encrypt response success");
			// Et on l'envoie
			res.send(toSend);
		},
		() => {
			// Une erreur est survenu.
			console.log("encrypt response fail");
			res.sendStatus(500); // internal error
		}
	)
});

/* Nous recevons une requete d'ajout de citation*/
app.post("/addcitation", (req, res) => {
	// On recupere la citation et l'utilisateur qui l'envoie
	let citation = req.citation.request.content;
	let user = req.citation.user;

	// On ajoute la citation
	db.addCitation(
		citation,
		user.token,
		() => {
			// La citation est bien ajoute
			console.log("ok")
			res.send("ok");
		},
		() => {
			// Une erreur est survenue
			console.log("500")
			res.sendStatus(500); // Internale error
		}
	);
});

/* Nous avons recu une requete de citation aleatoire */
app.post("/getcitation", (req, res) => {
	// On demande une citation aleatoire
	db.getRandomCitationForUser(
		req.citation.user.id,
		(citation) => {
			// On recoit notre citation, on prepare le chiffrement pour le retour
			cipher.init(
				() => {
					// On est pret a chiffre, on chiffre et on envoie
					let toSend = cipher.encrypt(citation.content);
					res.send(toSend);
				},
				() => {
					// Une erreur est survenue
					console.log("500")
					res.sendStatus(500); // Internale error
				}
			);
		},
		(err) => {
			// On n'a pas pu avoir de citation
			if(err.nothing == undefined) {
				// A cause d'une erreur
				res.sendStatus(500); // Internal error
			}
			else {
				// Parce qu'il n'y en a pas
				res.send("Pas de citation");
			}
		}
	);
});

let port = process.env.PORT;
if (port == null || port == "") {
  port = 8000;
}
app.listen(port);